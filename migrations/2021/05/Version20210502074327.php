<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210502074327 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE banning_system ADD author_user_id INT DEFAULT NULL, ADD is_canceled TINYINT(1) DEFAULT \'0\' NOT NULL, ADD reason_of_cancellation VARCHAR(255) NOT NULL, ADD author VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE banning_system ADD CONSTRAINT FK_62C3C6E8E2544CD6 FOREIGN KEY (author_user_id) REFERENCES user (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_62C3C6E8E2544CD6 ON banning_system (author_user_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE banning_system DROP FOREIGN KEY FK_62C3C6E8E2544CD6');
        $this->addSql('DROP INDEX IDX_62C3C6E8E2544CD6 ON banning_system');
        $this->addSql('ALTER TABLE banning_system DROP author_user_id, DROP is_canceled, DROP reason_of_cancellation, DROP author');
    }
}
