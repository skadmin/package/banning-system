<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210504161137 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $translations = [
            ['original' => 'banning-system.overview', 'hash' => 'bc6db5c0b969766a5c931bdddc0c49d4', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Přehled banů', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.banning-system.overview.title', 'hash' => '7123a19c643b4d9fbe767d19d618bf54', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Bany|Přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.banning-system.overview.action.cancel.title', 'hash' => 'ffe720192cebd62ec63a7b6e13ea4371', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Ukončení banu', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.banning-system.overview.action.cancel.message', 'hash' => 'eff2b443e1a2a4dff3a01c2a59ae3909', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Popište proč hodláte předčasně ukončit ban', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.banning-system.overview.action.cancel.placeholder', 'hash' => '22390c3f52ec2bef69f00ebc3557f211', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.banning-system.overview.is-canceled', 'hash' => '1b7f6e832bc5cc3b6d5c40c745f4b725', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Ukončen', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.banning-system.overview.name', 'hash' => '5474a56acfaff292e8989f9d453b9b11', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.banning-system.overview.type', 'hash' => 'acb8c3a184e269a0873608e2c386d3a6', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Typ', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.banning-system.overview.author', 'hash' => '439cccaa213018a310dd2564b56e1232', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Autor', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.banning-system.overview.last-update-at', 'hash' => '168ce0bbb0c000eb3f3d39a94a3ac468', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Poslední úprava', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.banning-system.overview.validity-to', 'hash' => '31b690be4434dc23b3c6ff109150455d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Platnost do', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.banning-system.overview.value', 'hash' => '2e0ba82fb2ab9aa9b5595579001a5c50', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Hodnota', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.banning-system.overview.note', 'hash' => 'a434e02635869062594d02b3623e0c4a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Důvod', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.banning-system.overview.entity', 'hash' => 'a150ae75f5f7cb37407d0454d4581d76', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Entita', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.banning-system.overview.ip-address', 'hash' => 'd7b29d124483abcb3430279f354bf853', 'module' => 'admin', 'language_id' => 1, 'singular' => 'IP adresa', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.banning-system.overview.type.ban', 'hash' => 'fa6caf51c714219e6668e6abc91f926b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Ban', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.banning-system.overview.action.cancel', 'hash' => 'b3b72b7ed4d392d2171688dd19a4e4d7', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Ukončit ban', 'plural1' => '', 'plural2' => ''],
            ['original' => 'banning-system.type.ban', 'hash' => 'def736c8c9cbfb4238db89669612e595', 'module' => 'admin', 'language_id' => 1, 'singular' => 'BAN', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.banning-system.overview.action.flash.cancle.success', 'hash' => '5ce2964096387e79db81e9e4f7f7cd99', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Ban byl úspěšně předčasně ukončen.', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema): void
    {
    }
}
