<?php

declare(strict_types=1);

namespace Skadmin\BanningSystem\Doctrine\BanningSystem;

use App\Model\Doctrine\User\User;
use DateTimeInterface;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Nette\Utils\DateTime;
use SkadminUtils\DoctrineTraits\Entity;

use function time;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class BanningSystem
{
    use Entity\BaseEntity;
    use Entity\Note;
    use Entity\Name;
    use Entity\Value;

    #[ORM\Column]
    private string $type;

    #[ORM\Column(nullable: true)]
    protected ?string $entity;

    #[ORM\Column(nullable: true)]
    protected ?string $entityId;

    #[ORM\Column]
    private string $ipAddress;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private DateTimeInterface $validityTo;

    #[ORM\Column(options: ['default' => false])]
    private bool $isCanceled = false;

    #[ORM\Column]
    private string $reasonOfCancellation = '';

    #[ORM\Column]
    private string $author;

    #[ORM\ManyToOne(targetEntity: User::class, cascade: ['persist'])]
    #[ORM\JoinColumn(onDelete: 'SET NULL')]
    private ?User $authorUser;

    public function create(?User $adminUser, string $type, string $name, string $entity, string $entityId, string $ipAddress, string $value, string $note, ?DateTimeInterface $validityTo = null): void
    {
        $this->authorUser = $adminUser;
        $this->author     = $adminUser instanceof User ? $adminUser->getFullName() : 'System';

        $this->type      = $type;
        $this->name      = $name;
        $this->entity    = $entity;
        $this->entityId  = $entityId;
        $this->ipAddress = $ipAddress;

        $this->update($value, $note, $validityTo);
    }

    public function update(string $value, string $note, ?DateTimeInterface $validityTo = null): void
    {
        $this->value = $value;
        $this->note  = $note;

        if (! $validityTo instanceof DateTimeInterface) {
            $validityTo = new DateTime();
            $validityTo->modify('+99 years');
        }

        $this->validityTo = $validityTo;
    }

    public function cancle(string $reason): void
    {
        $this->reasonOfCancellation = $reason;
        $this->isCanceled           = true;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getEntity(): ?string
    {
        return $this->entity;
    }

    public function getEntityId(): ?string
    {
        return $this->entityId;
    }

    public function getIpAddress(): string
    {
        return $this->ipAddress;
    }

    public function getValidityTo(): DateTimeInterface
    {
        return $this->validityTo;
    }

    public function isCanceled(): bool
    {
        return $this->isCanceled;
    }

    public function getReasonOfCancellation(): string
    {
        return $this->reasonOfCancellation;
    }

    public function getAuthor(): string
    {
        return $this->author;
    }

    public function isValid(): bool
    {
        return $this->validityTo->getTimestamp() > time() && ! $this->isCanceled();
    }
}
