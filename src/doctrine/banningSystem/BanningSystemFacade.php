<?php

declare(strict_types=1);

namespace Skadmin\BanningSystem\Doctrine\BanningSystem;

use DateTimeInterface;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Nette\Security\User;
use Nette\Utils\DateTime;
use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\BanningSystem\Events\EventBanningSystemCancleBan;
use SkadminUtils\DoctrineTraits\Facade;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

use function assert;
use function intval;
use function sprintf;

final class BanningSystemFacade extends Facade
{
    private EventDispatcherInterface $eventDispatcher;
    private User                     $user;

    public function __construct(EntityManagerDecorator $em, EventDispatcherInterface $eventDispatcher, User $user)
    {
        parent::__construct($em);
        $this->eventDispatcher = $eventDispatcher;
        $this->table           = BanningSystem::class;

        $this->user = $user;
    }

    /**
     * @param string|int $entityId
     */
    public function find(string $type, string $entity, $entityId): ?BanningSystem
    {
        $this->cancleExpireBan();

        return $this->em
            ->getRepository($this->table)
            ->findOneBy([
                'isCanceled' => false,
                'type'       => $type,
                'entity'     => $entity,
                'entityId'   => $entityId,
            ]);
    }

    /**
     * @return array<BanningSystem>
     */
    public function findForEntityByType(string $type, string $entity): array
    {
        $this->cancleExpireBan();

        return $this->em
            ->getRepository($this->table)
            ->findBy([
                'isCanceled' => false,
                'type'       => $type,
                'entity'     => $entity,
            ]);
    }

    /**
     * @param string|int $entityId
     * @param string|int $value
     */
    public function create(string $type, string $name, string $entity, $entityId, string $ipAddress, $value, string $note, ?DateTimeInterface $validityTo = null): BanningSystem
    {
        $banningSystem = $this->find($type, $entity, $entityId);

        if ($banningSystem instanceof BanningSystem) {
            return $this->update($banningSystem, $value, $note, $validityTo);
        } else {
            $banningSystem = $this->get();
        }

        $adminUser = $this->user->getIdentity();
        if (! $adminUser instanceof \App\Model\Doctrine\User\User) {
            $adminUser = null;
        }

        $banningSystem->create($adminUser, $type, $name, $entity, (string) $entityId, $ipAddress, (string) $value, $note, $validityTo);

        $this->em->persist($banningSystem);
        $this->em->flush();

        return $banningSystem;
    }

    /**
     * @param BanningSystem|int $banningSystem
     * @param string|int        $value
     */
    public function update($banningSystem, $value, string $note, ?DateTimeInterface $validityTo = null): BanningSystem
    {
        if (! $banningSystem instanceof BanningSystem) {
            $banningSystem = $this->get(intval($banningSystem));
        }

        $banningSystem->update((string) $value, $note, $validityTo);

        $this->em->flush();

        return $banningSystem;
    }

    /**
     * @param BanningSystem|int $banningSystem
     */
    public function remove($banningSystem): void
    {
        if (! $banningSystem instanceof BanningSystem) {
            $banningSystem = $this->get(intval($banningSystem));
        }

        if (! $banningSystem->isLoaded()) {
            return;
        }

        $this->em->remove($banningSystem);
        $this->em->flush();
    }

    /**
     * @param BanningSystem|int $banningSystem
     */
    public function cancle($banningSystem, string $reason, ?string $author = null): void
    {
        if (! $banningSystem instanceof BanningSystem) {
            $banningSystem = $this->get(intval($banningSystem));
        }

        if (! $banningSystem->isLoaded()) {
            return;
        }

        if ($author === null) {
            $adminUser = $this->user->getIdentity();
            if ($adminUser instanceof \App\Model\Doctrine\User\User) {
                $author = $adminUser->getFullName();
            } else {
                $author = 'System';
            }
        }

        $banningSystem->cancle(sprintf('%s: %s', $author, $reason));
        $this->em->flush();

        $this->eventDispatcher->dispatch(new EventBanningSystemCancleBan($banningSystem));
    }

    public function cancleExpireBan(): void
    {
        $repository = $this->em->getRepository($this->table);
        assert($repository instanceof EntityRepository);

        $qb = $repository->createQueryBuilder('a');

        $criteria = Criteria::create();
        $criteria->andWhere(Criteria::expr()->eq('a.isCanceled', false));
        $criteria->andWhere(Criteria::expr()->lte('a.validityTo', new DateTime()));

        $qb->addCriteria($criteria);

        foreach ($qb->getQuery()->getResult() as $ban) {
            $this->cancle($ban, 'expire', 'System');
        }
    }

    public function get(?int $id = null): BanningSystem
    {
        if ($id === null) {
            return new BanningSystem();
        }

        $banningSystem = parent::get($id);

        if ($banningSystem === null) {
            return new BanningSystem();
        }

        return $banningSystem;
    }
}
