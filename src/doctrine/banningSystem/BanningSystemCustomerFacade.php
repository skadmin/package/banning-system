<?php

declare(strict_types=1);

namespace Skadmin\BanningSystem\Doctrine\BanningSystem;

use DateTimeInterface;
use Nettrine\ORM\EntityManagerDecorator;
use SkadminUtils\DoctrineTraits\Facade;

use function intval;

final class BanningSystemCustomerFacade extends Facade
{
    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = BanningSystem::class;
    }

    public function find(string $type, string $entity, string|int $entityId): ?BanningSystem
    {
        return $this->em
            ->getRepository($this->table)
            ->findOneBy([
                'isCanceled' => false,
                'type'       => $type,
                'entity'     => $entity,
                'entityId'   => $entityId,
            ]);
    }

    public function create(string $type, string $name, string $entity, string|int $entityId, string $ipAddress, string|int $value, string $note, ?DateTimeInterface $validityTo = null): BanningSystem
    {
        $banningSystem = $this->find($type, $entity, $entityId);

        if ($banningSystem instanceof BanningSystem) {
            return $this->update($banningSystem, $value, $note, $validityTo);
        } else {
            $banningSystem = $this->get();
        }

        $banningSystem->create(null, $type, $name, $entity, (string) $entityId, $ipAddress, (string) $value, $note, $validityTo);

        $this->em->persist($banningSystem);
        $this->em->flush();

        return $banningSystem;
    }

    public function update(BanningSystem|int $banningSystem, string|int $value, string $note, ?DateTimeInterface $validityTo = null): BanningSystem
    {
        if (! $banningSystem instanceof BanningSystem) {
            $banningSystem = $this->get(intval($banningSystem));
        }

        $banningSystem->update((string) $value, $note, $validityTo);

        $this->em->flush();

        return $banningSystem;
    }
}
