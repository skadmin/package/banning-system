<?php

declare(strict_types=1);

namespace Skadmin\BanningSystem\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Doctrine\Common\Collections\Criteria;
use Nette\ComponentModel\IContainer;
use Nette\Http\Request;
use Nette\Security\User;
use Nette\Utils\Arrays;
use Nette\Utils\Html;
use Nette\Utils\Strings;
use Skadmin\BanningSystem\BaseControl;
use Skadmin\BanningSystem\Doctrine\BanningSystem\BanningSystem;
use Skadmin\BanningSystem\Doctrine\BanningSystem\BanningSystemFacade;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Translator\Translator;
use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

use function intval;
use function sprintf;
use function trim;

class Overview extends GridControl
{
    use APackageControl;

    /** @var callable[] */
    public array                $onModifyGrid;
    private Request             $request;
    private BanningSystemFacade $facade;
    private LoaderFactory       $webLoader;

    public function __construct(Request $request, BanningSystemFacade $facade, Translator $translator, User $user, LoaderFactory $webLoader)
    {
        parent::__construct($translator, $user);

        $this->request   = $request;
        $this->facade    = $facade;
        $this->webLoader = $webLoader;
    }

    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        return [];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [$this->webLoader->createJavaScriptLoader('bootbox')];
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');
        $template->drawBox = $this->drawBox;
        $template->render();
    }

    public function getTitle(): string
    {
        return 'grid.banning-system.overview.title';
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        $this->facade->cancleExpireBan();
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel()->addOrderBy('a.id', Criteria::DESC));

        // DATA
        $translator = $this->translator;
        $dialYesNo  = Arrays::map(Constant::DIAL_YES_NO, static function ($text) use ($translator): string {
            return $translator->translate($text);
        });

        // COLUMNS
        $grid->addColumnText('name', 'grid.banning-system.overview.name');
        $grid->addColumnText('isCanceled', 'grid.banning-system.overview.is-canceled')
            ->setRenderer(static function (BanningSystem $bs) use ($translator): Html {
                $result = new Html();

                $isCanceled = Html::el('span')
                    ->setText($translator->translate(Constant::DIAL_YES_NO[intval($bs->isCanceled())]));

                $result->addHtml($isCanceled);

                if ($bs->isCanceled()) {
                    $info = Html::el('small', [
                        'class'       => 'fas fa-fw fa-info ml-1',
                        'data-toggle' => 'tooltip',
                        'title'       => $bs->getReasonOfCancellation(),
                    ]);

                    $result->addHtml($info);
                }

                return $result;
            })
            ->setAlign('center');
        $grid->addColumnText('type', 'grid.banning-system.overview.type')
            ->setRenderer(fn (BanningSystem $bs) => $this->translator->translate(sprintf('grid.banning-system.overview.type.%s', $bs->getType())));
        $grid->addColumnText('author', 'grid.banning-system.overview.author');
        $grid->addColumnDateTime('lastUpdateAt', 'grid.banning-system.overview.last-update-at')
            ->setFormat('d.m.Y H:i')
            ->setAlign('center')
            ->setDefaultHide();
        $grid->addColumnDateTime('validityTo', 'grid.banning-system.overview.validity-to')
            ->setFormat('d.m.Y H:i')
            ->setAlign('center');
        $grid->addColumnText('value', 'grid.banning-system.overview.value')
            ->setDefaultHide();
        $grid->addColumnText('note', 'grid.banning-system.overview.note');
        $grid->addColumnText('entity', 'grid.banning-system.overview.entity')
            ->setRenderer(function (BanningSystem $banningSystem): string {
                if ($banningSystem->getEntity() === null && $banningSystem->getEntityId() === null) {
                    return '--';
                }

                $entintyName = sprintf('grid.banning-system.overview.entity - %s', Strings::webalize($banningSystem->getEntity()));

                return sprintf('%s:%s', $this->translator->translate($entintyName), $banningSystem->getEntityId());
            })
            ->setDefaultHide();
        $grid->addColumnText('ipAddress', 'grid.banning-system.overview.ip-address')
            ->setDefaultHide();

        // fitler
        $grid->addFilterText('name', 'grid.banning-system.overview.name');
        $grid->addFilterSelect('isCanceled', 'grid.banning-system.overview.is-canceled', $dialYesNo)
            ->setPrompt(Constant::PROMTP);
        $grid->addFilterText('author', 'grid.banning-system.overview.author');
        $grid->addFilterText('ipAddress', 'grid.banning-system.overview.ip-address');
        $grid->addFilterText('value', 'grid.banning-system.overview.value');
        $grid->addFilterText('note', 'grid.banning-system.overview.note');

        // row modify
        $grid->setRowCallback(static function (BanningSystem $bs, Html $tr): void {
            $tr->addClass($bs->isValid() ? 'border-left-success' : 'border-left-danger');
        });

        // hideable columns
        $grid->setColumnsHideable();

        // modify grid
        $this->onModifyGrid($grid);

        // action
        if ($this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $grid->addActionCallback('cancel', 'grid.banning-system.overview.action.cancel')
                ->setIcon('times')
                ->setClass('btn btn-xs btn-primary')
                ->setDataAttribute('bootbox', 'prompt-datagrid')
                ->setDataAttribute('bootbox-input-type', 'textarea')
                ->setDataAttribute('bootbox-message', $this->translator->translate('grid.banning-system.overview.action.cancel.message'))
                ->setDataAttribute('bootbox-title', $this->translator->translate('grid.banning-system.overview.action.cancel.title'))
                ->setDataAttribute('bootbox-placeholder', $this->translator->translate('grid.banning-system.overview.action.cancel.placeholder'))
                ->setDataAttribute('bootbox-link', $this->link('cancleBan!'));
        }

        $grid->allowRowsAction('cancel', static fn (BanningSystem $bs) => $bs->isValid());

        // OTHER
        $grid->setDefaultFilter(['isCanceled' => 0]);

        return $grid;
    }

    public function handleCancleBan(): void
    {
        $fileId = intval($this->request->getQuery('id'));
        $reason = $this->request->getQuery('result');

        if ($fileId <= 0 || trim($reason) === '') {
            return;
        }

        $banningSystem = $this->facade->get(intval($fileId));
        $presenter     = $this->getPresenterIfExists();

        if (! $banningSystem->isLoaded()) {
            if ($presenter !== null) {
                $presenter->flashMessage('grid.banning-system.overview.action.flash.cancle.fail', Flash::DANGER);
            }
        } else {
            $this->facade->cancle($banningSystem, $reason);

            if ($presenter !== null) {
                $presenter->flashMessage('grid.banning-system.overview.action.flash.cancle.success', Flash::SUCCESS);
            }
        }

        $this['grid']->reload();
    }
}
