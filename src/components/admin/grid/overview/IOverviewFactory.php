<?php

declare(strict_types=1);

namespace Skadmin\BanningSystem\Components\Admin;

interface IOverviewFactory
{
    public function create(): Overview;
}
