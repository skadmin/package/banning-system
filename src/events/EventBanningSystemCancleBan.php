<?php

declare(strict_types=1);

namespace Skadmin\BanningSystem\Events;

use Skadmin\BanningSystem\Doctrine\BanningSystem\BanningSystem;
use Symfony\Contracts\EventDispatcher\Event;

class EventBanningSystemCancleBan extends Event
{
    private BanningSystem $banningSystem;

    public function __construct(BanningSystem $banningSystem)
    {
        $this->banningSystem = $banningSystem;
    }

    public function getBanningSystem(): BanningSystem
    {
        return $this->banningSystem;
    }
}
